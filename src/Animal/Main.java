package Animal;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<Animal> animals = new ArrayList<Animal>();
        animals.add(new Cat());
        animals.add(new Cat());
        animals.add(new Dog());
        animals.add(new Dog());

        for(Animal animal : animals)
            animal.Voice();

        Animal someAnimal = animals.get(2);
        someAnimal.Sleep();
        someAnimal = animals.get(0);
        someAnimal.Sleep();

        System.out.println();
        for(Animal animal : animals)
            animal.Voice();

        someAnimal = animals.get(0);
        someAnimal.Eat();

        System.out.println();
        for(Animal animal : animals)
            animal.Voice();
    }

}
