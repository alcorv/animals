package Animal;

public abstract class Animal{
    protected Boolean isSleep;
    public Animal(){ isSleep = false; }
    public abstract void Voice();
    public void Sleep(){ isSleep = true; }
    public void Eat() { isSleep = false; }
}
